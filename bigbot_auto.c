#pragma once config(I2C_Usage, I2C1, i2cSensors)
#pragma once config(Sensor, I2C_1,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_2,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_3,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_4,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Motor,  port1,           LEFT_DRIVE,    tmotorVex393_HBridge, openLoop, encoderPort, I2C_4)
#pragma once config(Motor,  port2,           LEFT_DRIVE2,   tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port3,           LEFT_TOP_SHOOT, tmotorVex393_MC29, openLoop, encoderPort, I2C_1)
#pragma once config(Motor,  port4,           LEFT_BOT_SHOOT, tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port5,           RIGHT_TOP_SHOOT, tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port6,           RIGHT_BOT_SHOOT, tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port7,           RIGHT_DRIVE,   tmotorVex393_MC29, openLoop, encoderPort, I2C_2)
#pragma once config(Motor,  port8,           RIGHT_DRIVE2,  tmotorVex393_MC29, openLoop)
#pragma once config(Motor,  port9,           LOADER,        tmotorVex393_MC29, openLoop)
#pragma once config(Motor,  port10,          FEEDER,        tmotorVex393_HBridge, openLoop, encoderPort, I2C_3)

void DriveTrain(signed char left,signed char right){
		motor[LEFT_DRIVE] = left;
		motor[LEFT_DRIVE2] = left;
		motor[RIGHT_DRIVE] = right;
		motor[RIGHT_DRIVE2] = right;
}

// Drive foward using encoders to adjust as it goes along
// Precond: Feet, the distance in feet to drive foward
void DriveFoward(float feet){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to feet
	const float ratio = 1;
	float distance = 0;

	while(distance < feet){
		int left = nMotorEncoderRaw[LEFT_DRIVE];
		int right = nMotorEncoderRaw[RIGHT_DRIVE];
		if(abs(left) > abs(right) + 100){
				DriveTrain(-85,127);
				sleep(100);
		}
		else if (right > left + 100){
				DriveTrain(-127,85);
				sleep(100);
		}
		else{
				DriveTrain(-127,127);
				sleep(100);
		}
		DriveTrain(0,0);
		distance = ratio*nMotorEncoderRaw[LEFT_DRIVE];
	}
}

// Drive foward using encoders to adjust as it goes along
// Precond: Feet, the distance in feet to drive foward
void DriveBackward(float feet){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to feet
	const float ratio = 1;
	float distance = 0;

	while(distance < feet){
		int left = nMotorEncoderRaw[LEFT_DRIVE];
		int right = nMotorEncoderRaw[RIGHT_DRIVE];
		if(abs(left) > abs(right) + 100){
				DriveTrain(-85,-127);
				sleep(100);
		}
		else if (right > left + 100){
				DriveTrain(-127,-85);
				sleep(100);
		}
		else{
				DriveTrain(-127,-127);
				sleep(100);
		}
		DriveTrain(0,0);
		distance = ratio*nMotorEncoderRaw[LEFT_DRIVE];
	}
}


// Rotate a relative amount
// Precond: dir, the degrees to rotate by
void Turn(int dir){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to degrees
	const float ratio = 1;
	float cur_dir = 0;

	while(abs(cur_dir) < abs(dir)){
		int encoder = nMotorEncoderRaw[LEFT_DRIVE];
		if(cur_dir > dir){
				DriveTrain(-127,127);
		}
		else{
				DriveTrain(127,-127);
		}
		cur_dir = encoder*ratio;
	}
}

void AutoShoot(){

}

void Autonomous(){
	DriveFoward(1);
	Turn(-90);
	motor[FEEDER] = 127;
	DriveFoward(3);
	motor[FEEDER] = 0;
	Turn(-90);
	motor[FEEDER] = 127;
	DriveFoward(2);
	motor[FEEDER] = 0;
	DriveBackward(6);
	Turn(135);
}
