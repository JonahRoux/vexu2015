#pragma once config(I2C_Usage, I2C1, i2cSensors)
#pragma once config(Sensor, I2C_1,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_2,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_3,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Sensor, I2C_4,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign)
#pragma once config(Motor,  port1,           RIGHT_BOT_SHOOT, tmotorVex393_HBridge, openLoop)
#pragma once config(Motor,  port2,           RIGHT_DRIVE,   tmotorVex393_MC29, openLoop, encoderPort, I2C_1)
#pragma once config(Motor,  port3,           LEFT_DRIVE,    tmotorVex393_MC29, openLoop, encoderPort, I2C_3)
#pragma once config(Motor,  port4,           RIGHT_TOP_SHOOT, tmotorVex393_MC29, openLoop, reversed, encoderPort, I2C_2)
#pragma once config(Motor,  port5,           INTAKE,        tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port6,           FEEDER,        tmotorVex393_MC29, openLoop, encoderPort, None)
#pragma once config(Motor,  port7,           LEFT_TOP_SHOOT, tmotorVex393_MC29, openLoop, reversed, encoderPort, I2C_4)
#pragma once config(Motor,  port8,           LEFT_BOT_SHOOT, tmotorVex393_MC29, openLoop, reversed)
#pragma once config(Motor,  port10,          LOADER,        tmotorVex393_HBridge, openLoop)

void DriveTrain(signed char left,signed char right){
		motor[LEFT_DRIVE] = left;
		motor[RIGHT_DRIVE] = right;
}
void RunShooter(signed char value){
		motor[LEFT_TOP_SHOOT] = value;
		motor[LEFT_BOT_SHOOT] = value;
		motor[RIGHT_TOP_SHOOT] = -value;
		motor[RIGHT_BOT_SHOOT] = -value;
}

// Drive foward using encoders to adjust as it goes along
// Precond: Feet, the distance in feet to drive foward
void DriveFoward(float feet){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to feet
	const float ratio = 1;
	float distance = 0;

	while(distance < feet){
		int left = nMotorEncoderRaw[LEFT_DRIVE];
		int right = nMotorEncoderRaw[RIGHT_DRIVE];
		if(abs(left) > abs(right) + 100){
				DriveTrain(-85,127);
				sleep(100);
		}
		else if (right > left + 100){
				DriveTrain(-127,85);
				sleep(100);
		}
		else{
				DriveTrain(-127,127);
				sleep(100);
		}
		DriveTrain(0,0);
		distance = ratio*nMotorEncoderRaw[LEFT_DRIVE];
	}
}

// Drive foward using encoders to adjust as it goes along
// Precond: Feet, the distance in feet to drive foward
void DriveBackward(float feet){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to feet
	const float ratio = 1;
	float distance = 0;

	while(distance < feet){
		int left = nMotorEncoderRaw[LEFT_DRIVE];
		int right = nMotorEncoderRaw[RIGHT_DRIVE];
		if(abs(left) > abs(right) + 100){
				DriveTrain(-85,-127);
				sleep(100);
		}
		else if (right > left + 100){
				DriveTrain(-127,-85);
				sleep(100);
		}
		else{
				DriveTrain(-127,-127);
				sleep(100);
		}
		DriveTrain(0,0);
		distance = ratio*nMotorEncoderRaw[LEFT_DRIVE];
	}
}


// Rotate a relative amount
// Precond: dir, the degrees to rotate by
void Turn(int dir){
	resetMotorEncoder(LEFT_DRIVE);
	resetMotorEncoder(RIGHT_DRIVE);

	//Ratio of clicks to degrees
	const float ratio = 1;
	float cur_dir = 0;

	while(abs(cur_dir) < abs(dir)){
		int encoder = nMotorEncoderRaw[LEFT_DRIVE];
		if(cur_dir > dir){
				DriveTrain(-127,127);
		}
		else{
				DriveTrain(127,-127);
		}
		cur_dir = encoder*ratio;
	}
}

void AutoShoot(){
	static int prev_encoder = 0;
	static int encoder = 0;
	int time = time1[T1];
	if(time > 1000){
		encoder = (nMotorEncoderRaw[LEFT_TOP_SHOOT]-prev_encoder)/(time1[T1]/1000);
	}
	RunShooter(127);
	if(encoder > 700){
		motor[FEEDER] = 127;
		motor[LOADER] = 127;
	}
}

void Autonomous(){
	DriveFoward(1);
}
